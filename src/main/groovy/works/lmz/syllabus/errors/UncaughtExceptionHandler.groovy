package works.lmz.syllabus.errors

import groovy.transform.CompileStatic
import works.lmz.common.jackson.JacksonHelperApi
import works.lmz.common.stereotypes.SingletonBean
import works.lmz.syllabus.payload.ErrorResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.inject.Inject

/**
 * A default exception handler that catches all that haven't
 * already been accounted for in other handlers. Stops any
 * errors bubbling up to the connection layer.
 */

@CompileStatic
@SingletonBean
public class UncaughtExceptionHandler implements SyllabusExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(UncaughtExceptionHandler)

	public static final String ERROR_UNKNOWN = 'error.unknown'
	public static final String ERROR_MESSAGE = 'An unhandled exception has been thrown by the application'

	@Inject
	protected JacksonHelperApi jacksonHelperApi

	/**
	 * Returns a basic error response to avoid exceptions bubbling any further toward the connection.
	 * @param exception The exception being thrown.
	 */
	public ErrorResponse handleError(Exception exception) {
		Map errorIdentifier = [uuid: UUID.randomUUID().toString()]
		log.error("$ERROR_MESSAGE: ${jacksonHelperApi.jsonSerialize(errorIdentifier)}", exception)
		return new ErrorResponse(error: ERROR_UNKNOWN, context: errorIdentifier)
	}

}
