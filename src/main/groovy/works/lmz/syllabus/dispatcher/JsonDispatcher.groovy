package works.lmz.syllabus.dispatcher

import groovy.transform.CompileStatic
import works.lmz.common.jackson.JacksonException
import works.lmz.common.jackson.JacksonHelperApi
import works.lmz.common.stereotypes.SingletonBean
import works.lmz.syllabus.SyllabusContext
import works.lmz.syllabus.errors.TransmissionException
import works.lmz.syllabus.events.EventDispatcher
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.inject.Inject

/**
 *
 * @author: Richard Vowles - https://plus.google.com/+RichardVowles
 */
@SingletonBean
@CompileStatic
class JsonDispatcher implements Dispatcher {
	private Logger log = LoggerFactory.getLogger(getClass())
	@Inject EventDispatcher eventDispatcher
	@Inject JacksonHelperApi jacksonHelperApi

	@Override
	List<String> supports() {
		return ['application/json', 'application/javascript']
	}

	protected Object provideObject(SyllabusContext context) {
		if (context.currentHandle.serializeObject == null) {
			return null
		}

		// try to deserialize the incoming request into the handler's input type
		try {
			return jacksonHelperApi.jsonDeserialize(context.requestBody, context.currentHandle.serializeObject)
		}
		catch (JacksonException jEx) {
			log.warn("Jackson was unable to deserialize JSON into ${context.currentHandle.serializeObject.simpleName}, json: $context.requestBody", jEx)

			// marshalling went wrong
			throw new TransmissionException(
				String.format('Could not marshal requestBody into `%s` because: %s', context.currentHandle.serializeObject.simpleName, jEx.message)
			)
		}
	}

	@Override
	Object dispatch(SyllabusContext context) {
		return eventDispatcher.dispatch(context, this.&provideObject)
	}
}
