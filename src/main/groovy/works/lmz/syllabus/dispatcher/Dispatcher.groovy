package works.lmz.syllabus.dispatcher

import groovy.transform.CompileStatic
import works.lmz.syllabus.SyllabusContext

/**
 *
 * @author: Richard Vowles - https://plus.google.com/+RichardVowles
 */

@CompileStatic
interface Dispatcher {
	List<String> supports()
	Object dispatch(SyllabusContext context)
}
