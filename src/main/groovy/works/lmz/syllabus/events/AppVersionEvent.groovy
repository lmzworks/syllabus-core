package works.lmz.syllabus.events

import works.lmz.util.LmzAppVersion
import javax.inject.Inject

/**
 * This event simultaneously prevents the DI engine from throwing a fit when
 * it can't find any @Event-s on the classpath, and also provides an easy way
 * to check what version an application may be.
 *
 */

@Event(name = 'version', namespace = 'meta')
public class AppVersionEvent {

	@Inject LmzAppVersion appVersion

	/**
	 * Simply wraps the version in the required response object.
	 */
	public AppVersionResponse handle() {
		return new AppVersionResponse( version: appVersion.version )
	}

	/**
	 * The only response parameter should be the version
	 */
	public static class AppVersionResponse {
		public String version
	}
}
