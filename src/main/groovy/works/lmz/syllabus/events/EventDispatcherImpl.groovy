package works.lmz.syllabus.events

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import works.lmz.common.stereotypes.SingletonBean
import works.lmz.syllabus.ResponseCode
import works.lmz.syllabus.SyllabusContext
import works.lmz.syllabus.errors.SyllabusExceptionHandler
import works.lmz.syllabus.errors.TransmissionException
import works.lmz.syllabus.events.EventDispatcher.DecodeCallback
import works.lmz.syllabus.generator.EventHandlerConfig
import works.lmz.syllabus.hooks.AfterEvent
import works.lmz.syllabus.hooks.EventHookCollection
import works.lmz.syllabus.hooks.EventHookException
import works.lmz.syllabus.payload.ErrorResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import works.lmz.syllabus.errors.ErrorHandlerCollection

import javax.inject.Inject

/**
 *
 * @author: Richard Vowles - https://plus.google.com/+RichardVowles
 */
@SingletonBean
@CompileStatic
class EventDispatcherImpl implements EventDispatcher {

	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(EventDispatcher.class)

	/**
	 * Event collection bound here
	 */
	@Inject
	private EventHandlerCollection eventCollection

	/**
	 * Allows us to properly handle errors
	 */
	@Inject
	private ErrorHandlerCollection errorHandlerCollection

	/**
	 * Event hook collection
	 */
	@Inject
	private EventHookCollection eventHookCollection

	/**
	 * Dispatch the for a certain URL with a particular payload
	 *
	 * @param url is the url to dispatch to
	 * @param payload is the payload to pass into the event handler
	 */
	public Object dispatch(SyllabusContext context, DecodeCallback decodeCallback)
		throws TransmissionException, EventHookException {
		// figure out who should handle it
		context.handlerConfig = getEventHandlerByName(context.action, context.namespace)

		// handle the event and catch and try to do something with
		Object returnObject

		// run the @BeforeEvent annotated EventHooks for this event, it could change the SyllabusHandle
		try {
			runBeforeEventHooks(context)
		} catch (EventHookException eHe) {
			// if a EventHookException has been thrown, rethrow it, the outside world
			// is expecting it!
			throw eHe
		} catch (Exception beforeHookException) {
			returnObject = handleError(beforeHookException)
			return returnObject
		}

		// make sure we can actually call something
		if (!context.currentHandle) {
				throw new TransmissionException(
						"No event handler found for ${context.namespace}::${context.action} (v${context.version})",
						ResponseCode.NOT_FOUND
				)
		}

		try {
			returnObject = context.currentHandle.invoke(decodeCallback.decode(context), context)
		} catch (TransmissionException eh) {
			throw eh
		} catch (Exception ex) {
			returnObject = handleError(ex)
		}

		// put the returnObject into the context so any @AfterEvent hook can modify it
		context.responseObject = returnObject
		runAfterEventHooks(context)
		return context.responseObject
	}

	void runAfterEventHooks(SyllabusContext context) {
		eventHookCollection.runHooks(AfterEvent, context)
	}

	@CompileStatic(TypeCheckingMode.SKIP)
	protected ErrorResponse handleError(Exception ex) {
		// something "expected" happened, let's go looking for an error handler
		SyllabusExceptionHandler<? extends Exception> exHandler = errorHandlerCollection.getHandlerFor(ex)

		if (!exHandler) {
			log.error('No exception handler found for', ex)
			return null
		}

		return exHandler.handleError(ex)
	}
/**
 * Run before event hooks
 *
 * @param eventHandler is the current event handler
 * @param namespace is the namespace to run the event hooks for
 */
	protected void runBeforeEventHooks(SyllabusContext context) {
		 eventHookCollection.runBeforeEventHooks(context)
	}

	/**
	 * Find an eventHandler instance by name
	 *
	 * @param action is the action to look for
	 * @param namespace is the namespace to be looking in
	 * @return is null when there is no such event handler, or an eventHandler instance when found.
	 */
	protected EventHandlerConfig getEventHandlerByName(String action, String namespace) {
		return eventCollection.findByName(action, namespace)
	}
}
