package works.lmz.syllabus.hooks

import groovy.transform.CompileStatic
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import works.lmz.syllabus.SyllabusContext

/**
 * This event simultaneously prevents the DI engine from throwing a fit when
 * it can't find any @EventHook-s on the classpath, and also provides an easy
 * way to see the contexts are doing.
 *
 */

@BeforeEvent
@CompileStatic
class ContextLoggerEventHook implements EventHook {

	private static final Logger log = LoggerFactory.getLogger(ContextLoggerEventHook.class)

	/**
	 * Logs the SyllabusContext
	 *
	 * @param event is the event handler that is about to be invoked
	 *
	 */

	@Override
	void executeHook(SyllabusContext context ) {
		if ( log.debugEnabled ) {
			log.debug context.toString()
		}
	}
}
