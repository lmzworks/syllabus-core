package works.lmz.syllabus.errors

import groovy.transform.CompileStatic
import works.lmz.syllabus.payload.ErrorResponse
import works.lmz.common.jackson.JacksonHelper
import org.junit.Before
import org.junit.Test

@CompileStatic
public class UncaughtExceptionHandlerTest {

	UncaughtExceptionHandler target

	@Before
	public void setUp() throws Exception {
		target = new UncaughtExceptionHandler()
		target.jacksonHelperApi = new JacksonHelper()
	}

	@Test
	public void returnsErrorWithUuid() throws Exception {
		Exception exception = new Exception("This will only be seen in the log.")

		//-----------------------------------------------------
		ErrorResponse response = target.handleError(exception)
		//-----------------------------------------------------

		assert response.error == UncaughtExceptionHandler.ERROR_UNKNOWN
		assert response.context instanceof Map
		assert response.context.containsKey('uuid')
	}
}
