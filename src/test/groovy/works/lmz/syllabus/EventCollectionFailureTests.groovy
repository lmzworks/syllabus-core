package works.lmz.syllabus

import works.lmz.common.testrunner.BatheCommandLine
import works.lmz.common.testrunner.SimpleTestRunner
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.context.support.ClassPathXmlApplicationContext

/**
 *
 * @author: Richard Vowles - https://plus.google.com/+RichardVowles
 */
@BatheCommandLine(["-Pclasspath:/test.properties"])
@RunWith(SimpleTestRunner.class)
class EventCollectionFailureTests {
	final shouldFail = new GroovyTestCase().&shouldFail

	@Test
	public void shouldBlow() {
		println ( 'lmz.devmode:="' + System.getProperty( 'lmz.devmode' )+'"' )
		shouldFail(RuntimeException) {
			new ClassPathXmlApplicationContext("/event-failure.xml")
		}
	}
}
